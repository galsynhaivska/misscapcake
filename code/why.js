import {cakes} from "../code/data.js";
import {localCakes, hideBag, clickShoppingBag} from "./function.js";


const modalBag = document.querySelector(".modal-bag");
hideBag();

//cakes in localStirage
const userCakes = localCakes(cakes);
//console.log(userCakes);

let order;
if(!window.localStorage.order){
    order = [];
}else{
    order = JSON.parse(window.localStorage.order);
}    
const orderBag = document.querySelector(".counter-shopping-bag");
//orderBag.innerText = order.length;   // якщо рахуємо кількість видів кейків
orderBag.innerText = `${ order.reduce((total, item) => {
    total += parseInt(item.quantity);
    return total;
}, 0)}`;      // якщо рахуємо кількість кейків

document.querySelector(".shopping-bag").classList.remove("blocked");


// click on 'My bag'
document.querySelector(".shopping-bag").addEventListener("click", clickShoppingBag);

//click on 'ShopNow' and 'OurStory'
document.querySelectorAll(".shopping-btn").forEach(el => {
    el.addEventListener("click", hideBag);
});



