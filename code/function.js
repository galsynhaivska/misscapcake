import {cakes} from "../code/data.js";
//MyBag function
export function localCakes(array) {
    if(!window.localStorage.userCakes){
        window.localStorage.userCakes = JSON.stringify(array.map(el => {
               return {id: el.id,
                cakeName: el.cakeName,
                quantity: 0,
                price: el.price, 
                image: el.image,
                }
        }));
    }    
    return JSON.parse(window.localStorage.userCakes);
}

//hide MyBag
export function hideBag(){
    const modalBag =  document.querySelector(".modal-bag");
    modalBag.style.display = "none";
}

//create Card on main page
export function createCard(item){
    return `
    <div class="item" id=${item.id}>
        <img class="item-img" src="./image/${item.image}" alt=${item.cakeName}>                    
        <h3 class="item-title">${item.cakeName}</h3>
        <p class="item-info">${item.info}</p>
        <div class="count-btn-add">
            <div class="counter">
                <button class="minus" type="button"> - </button>
                <div class="result"> 0 </div>
                <button class="plus" type="button"> + </button>
            </div>
            <div class="btn-add">
                <a class="shopping-btn add-to-card" href="">ADD TO CART</a>
            </div>
        </div>
    </div>
    `;
}
//change cake's quantity on page
export function changeItem(event) {
    console.log(JSON.parse(window.localStorage.userCakes))
    hideBag(document.querySelector(".modal-bag"));
    const divItem = event.target.parentElement.parentElement.parentElement;
        let array = JSON.parse(window.localStorage.userCakes),
        selectedCake = array.find((el) => el.id === divItem.id);
        console.log(selectedCake)
        let order;
        if(window.localStorage.order){
             order = JSON.parse(window.localStorage.order);
        }else{
             order = [];
        }
    //    console.log(order);
    if(event.target.className === "plus"){
        const result = event.target.previousElementSibling;
        if(parseInt(result.innerText) <= parseInt(cakes.find((el) => el.id === divItem.id).quantity)){
            result.innerText = parseInt(result.innerText) + 1;
            selectedCake.quantity = result.innerText;    
        }
    }else{
        const result = event.target.nextElementSibling;
        if( parseInt(result.innerText) > 0){
            result.innerText = parseInt(result.innerText) - 1;
            selectedCake.quantity = result.innerText; 
          
             //document.querySelector(".counter-shopping-bag").innerText = order.length;  // якщо рахуємо кількість видів кейків
            ////orderBag.innerText = order.length;    
            document.querySelector(".counter-shopping-bag").innerText = `${ order.reduce((total, item) => {
                total += parseInt(item.quantity);
                return total;
            }, 0)}`;      // якщо рахуємо кількість кейків  
        }
        if(parseInt(result.innerText) === 0){
         //   console.log(parseInt(result.innerText), result, divItem.id);
            order.forEach((el, i) =>{
                if(el.id === divItem.id){
                    console.log(el.id, i);
                    order.splice(i, 1);
                }
            }); 
            document.querySelector(".counter-shopping-bag").innerText = `${ order.reduce((total, item) => {
                total += parseInt(item.quantity);
                return total;
            }, 0)}`;      // якщо рахуємо кількість кейків  
         
        }   
   
    }     
    window.localStorage.userCakes = JSON.stringify(array);
    window.localStorage.order = JSON.stringify(order);
}


function hideModal(obj){
    obj.style.display = "none";
//    clearTimeout (timer);
}

let timer;

export function addToBagClick(event) {
    hideBag(document.querySelector(".modal-bag"));
    event.preventDefault();
    if(!window.localStorage.order){  
        window.localStorage.order = JSON.stringify([]);  
    }
    let order = JSON.parse(window.localStorage.order);
    
    const divItem = event.target.parentElement.parentElement.parentElement; 
    const quantity = event.target.parentElement.previousElementSibling.children[1];
    const addItem = localCakes(cakes).find(el => el.id === divItem.id);
    const findItem = order.find((el) => el.id === divItem.id);

        if(quantity.innerText !== '0'){
            if(order.find((el) => el.id === divItem.id)){
                order.forEach(el => {
                    if(el.id === divItem.id){
                        el.quantity = parseInt(el.quantity) + parseInt(quantity.innerText);
                    }
                });
            }else{
                order.push(addItem); 
            }     
            window.localStorage.order = JSON.stringify(order);
            //document.querySelector(".counter-shopping-bag").innerText = order.length;  // якщо рахуємо кількість видів кейків
            ////orderBag.innerText = order.length;   
            document.querySelector(".counter-shopping-bag").innerText = `${ order.reduce((total, item) => {
                total += parseInt(item.quantity);
                return total;
            }, 0)}`;      // якщо рахуємо кількість кейків
        }else{
            const divModal = document.querySelector(".div-modal");
            divModal.style.top = event.target.offsetTop + 45 + "px"; 
            divModal.style.left = event.target.offsetLeft  + "px"; 
            divModal.style.display = "block";
            timer = setTimeout(() => { 
                hideModal(divModal);
            }, 1000);
        }         
}

export const clickShoppingBag = (event) => {
   // console.log(document.querySelector(".shopping-bag > a"))
    if(!(document.querySelector(".shopping-bag > a")).classList.contains("blocked")){
        document.querySelector(".shopping-bag > a").classList.add("blocked");
        const modalBag = document.querySelector(".modal-bag"),
            btnCheckOut = document.querySelector(".btn-checkout"),
            btnClose = document.querySelector(".button-close"),
            tableBody = document.querySelector(".table-body"),
            totalDiv = document.querySelector(".total-price");
        if(!window.localStorage.order){  
            window.localStorage.order = JSON.stringify([]);  
        }
        let order = JSON.parse(window.localStorage.order);
        const bagFilter = order.filter((el) => parseInt(el.quantity) !== 0 );  
    //    console.log(bagFilter)
    //    console.log(document.location.pathname.search("why"))
        tableBody.innerText ="";
        if(document.location.pathname.search("why") === -1){
            tableBody.insertAdjacentHTML("beforeend", bagFilter.map((item) => {   
                return `
                    <tr>
                        <td class="data-1">
                            <img class="img-cake" src="./image/${item.image}" alt=${item.cakeName}>  
                        </td>
                        <td class="data-2">
                            <p class="cake-name">${item.cakeName} x ${item.quantity}</p>
                        </td>
                        <td class="data-3">
                            <p class="price-cake">$${(item.quantity * item.price).toFixed(2)}</p>
                        </td>
                    </tr> 
                `
            }).join(""));
        }
        if(document.location.pathname.search("why") !== -1){
            tableBody.insertAdjacentHTML("beforeend", bagFilter.map((item) => {   
            return `
                <tr>
                    <td class="data-1">
                        <img class="img-cake" src="../image/${item.image}" alt=${item.cakeName}>  
                    </td>
                    <td class="data-2">
                        <p class="cake-name">${item.cakeName} x ${item.quantity}</p>
                    </td>
                    <td class="data-3">
                        <p class="price-cake">$${(item.quantity * item.price).toFixed(2)}</p>
                    </td>
                </tr> 
            `
            }).join(""));
        }
 
        window.localStorage.order = JSON.stringify(bagFilter);
        totalDiv.innerText = `$${ order.reduce((total, item) => {
            total += item.price * item.quantity;
            return total
        }, 0).toFixed(2)}
        `;
    
        if(document.location.pathname.search("cart") === -1){
            event.preventDefault();
            modalBag.style.display = "block";
            
            btnClose.addEventListener("click", () => {
                modalBag.style.display = "none";
                document.querySelector(".shopping-bag > a").classList.remove("blocked");
            });

            btnCheckOut.addEventListener("click", () => {
                document.location.href = './cart';
            if(document.location.pathname.search("why") === -1){
                document.location.href = './cart';
            }else{
                console.log("=", document.location.pathname.search("why"))
                document.location.href = '../cart';
            }    
            });
        }else{
                document.querySelector(".shopping-bag").classList.add("blocked");
                event.preventDefault();   
                
        }  
    }
}

export function blockClass(arr, attr){
    arr.forEach(el => {
        if(attr === "add"){
            el.classList.add("blocked");
        } else if(attr === "remove"){
            el.classList.remove("blocked");
        }
        
    })

}



