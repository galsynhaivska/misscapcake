import {cakes} from "../code/data.js";
import {localCakes, hideBag, createCard, changeItem, addToBagClick, clickShoppingBag} from "./function.js";


const modalBag = document.querySelector(".modal-bag");
hideBag();

//cakes in localStorage
const userCakes = localCakes(cakes);
//console.log(userCakes);

let order;
if(!window.localStorage.order){
    order = [];
}else{
    order = JSON.parse(window.localStorage.order);
}    
const orderBag = document.querySelector(".counter-shopping-bag");
//orderBag.innerText = order.length;   // якщо рахуємо кількість видів кейків
orderBag.innerText = `${ order.reduce((total, item) => {
    total += parseInt(item.quantity);
    return total;
}, 0)}`;      // якщо рахуємо кількість кейків

document.querySelector(".shopping-bag").classList.remove("blocked");

//fill catalog-list
const list = document.querySelector(".catalog-list");
cakes.forEach((el) => {
    list.insertAdjacentHTML("beforeend", createCard(el));
});

//fill quantity fields
[...document.querySelectorAll(".item .result")].forEach(element =>{
    userCakes.forEach(el =>{
    if(el.id === element.parentElement.parentElement.parentElement.id ){    
        element.innerText = (el.quantity);
    }
   })
});

// click on "+" or "-"
[...document.querySelectorAll(".item")].forEach(element =>{
    element.querySelector(".plus").addEventListener("click", changeItem);
    element.querySelector(".minus").addEventListener("click", changeItem);
});

// click on "Add to cart"
for(let i = 0; i < list.children.length; i++){     
  //list.children[i].lastElementChild.lastElementChild === div.btn-count-add     
    list.children[i].lastElementChild.lastElementChild.addEventListener("click", addToBagClick);
}
 
// click on 'My bag'
document.querySelector(".shopping-bag").addEventListener("click", clickShoppingBag);

//click on 'ShopNow' and 'OurStory'
document.querySelectorAll(".shopping-btn").forEach(el => {
    el.addEventListener("click", hideBag);
});
document.querySelector(".story-button").addEventListener("click", hideBag);
